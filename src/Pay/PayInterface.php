<?php

declare ( strict_types = 1 )
	;

namespace Lib\Pay;

use Lib\AbstractInterface;

interface PayInterface extends AbstractInterface {
	/**
	 * 获取订单编号
	 *
	 * @return []
	 */
	public function getResource();
	
	/**
	 * 验证参数
	 */
	public function getValidateByPay(): array;
	
	/**
	 * 根据支付类型获取支付信息
	 *
	 * @return array
	 */
	public function getPayInfoByType(array $post, array $cookie): array;
	/**
	 * 获取支付配置
	 *
	 * @return array
	 */
	public function getPayConfig(array $post, array $cookie): array;
	
	/**
	 *
	 * @return boolean
	 */
	public function getIsPass();
	/**
	 *
	 * @return the $returnMonery
	 */
	public function getReturnMonery();
	/**
	 *
	 * @param number $returnMonery        	
	 */
	public function setReturnMonery($returnMonery);
	/**
	 *
	 * @return the $payData
	 */
	public function getPayData();
	/**
	 *
	 * @param field_type $payData        	
	 */
	public function setPayData($payData);
}