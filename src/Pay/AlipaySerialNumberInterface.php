<?php

declare ( strict_types = 1 )
	;

namespace Lib\Pay;

use Lib\AbstractInterface;

interface AlipaySerialNumberInterface extends AbstractInterface {
	/**
	 *
	 * @return \App\Models\Logic\Specific\unknown
	 */
	public function getAliOrderId();
	
	/**
	 * 处理支付宝订单
	 * 
	 * @param array $data        	
	 * @return bool
	 */
	public function parseAlipayConfig(array $data): bool;
	
	/**
	 * 余额回调处理
	 */
	public function parseByPay(array $data): bool;
}