<?php

declare ( strict_types = 1 )
	;

namespace Lib\Pay;

use Lib\AbstractInterface;

interface PayTypeInterface extends AbstractInterface {
	public function getRechargeType();
	
	/**
	 * 获取结果(支付列表)
	 */
	public function getPayType(): array;
	public function getPayName(array $data, string $splitKey): array;
	/**
	 *
	 * @return array
	 */
	public function isPayedCache(array $data, string $splitKey): array;
	/**
	 *
	 * @return Generator
	 */
	public function gePayNameByCash(array $dataSources, string $splitKey): array;
	
	/**
	 * 获取结果(支付列表)
	 */
	public function getPayTypeByWap(): array;
}