<?php

declare ( strict_types = 1 )
	;

namespace Lib\Integral;

use Lib\AbstractInterface;

interface IntegralInterface extends AbstractInterface {
	
	
	
	/**
	 *
	 * @name 会员积分明细日志
	 *      
	 *       会员积分明细日志
	 *       2018-01-06
	 */
	public function integralLog();
}