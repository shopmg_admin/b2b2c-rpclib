<?php

declare ( strict_types = 1 )
	;

namespace Lib\Integral;

use Lib\AbstractInterface;

interface IntegralGoodsInterface extends AbstractInterface {
	/**
	 *
	 * @return the $cache
	 */
	public function getCache();
	
	/**
	 * 最新兑换商品
	 */
	public function getNewList(): array;
	/**
	 * 获取商品关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 验证是否可兑换
	 */
	public function checkIsConvertibility(array $post): array;
	/**
	 * 获取积分商品的详细信息
	 */
	public function getIntegralGoodInfo(array $post);
	/**
	 * 获取积分商品
	 *
	 * @param array $post        	
	 * @return array
	 */
	public function getIntegralListBySearch(array $post): array;
}