<?php

declare ( strict_types = 1 )
	;

namespace Lib\Integral;

use Lib\AbstractInterface;

interface IntegralUseInterface extends AbstractInterface {
	
	
	
	
	
	/**
	 * 获取积分列表
	 */
	public function getIntegralList(array $post): array;
	/**
	 * 获取商品关联字段
	 * 
	 * @return string
	 */
	public function getSplitKeyByOrderId(): string;
	/**
	 * 添加积分记录
	 * 
	 * @return boolean
	 */
	public function addIntegral(array $data, float $payIntegral): array;
}