<?php

declare ( strict_types = 1 )
	;

namespace Lib\Freight;

use Lib\AbstractInterface;

interface FreightsInterface extends AbstractInterface {
	/**
	 *
	 * @return number
	 */
	public function getStoreId();
	/**
	 *
	 * @return []
	 */
	public function getIsPost();
	/**
	 *
	 * @return bool
	 */
	public function getIsAllPost();
	
	/**
	 * 验证数据(计算运费运算量大 特此简化验证操作)
	 * 
	 * @return bool
	 */
	public function getValidateSource(array $post);
	/**
	 * 获取运费配置
	 * 
	 * @return float
	 */
	public function getFreightTemplate();
}