<?php

declare ( strict_types = 1 )
	;

namespace Lib\Freight;

use Lib\AbstractInterface;

interface FreightSendInterface extends AbstractInterface {
	/**
	 * 获取配送地址
	 * 
	 * @return array
	 */
	public function getSendAddress(array $freightMode);
	/**
	 * 验证收货地址是否在配送区域内
	 * 
	 * @return bool
	 */
	public function userAddressIndexOfSendArea(array $data): array;
	/**
	 * 获取地区关联字段
	 */
	public function getRegionAddress();
}