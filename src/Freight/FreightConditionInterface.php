<?php

declare ( strict_types = 1 )
	;

namespace Lib\Freight;

use Lib\AbstractInterface;

interface FreightConditionInterface extends AbstractInterface {
	
	/**
	 * 获取 运费配置信息
	 */
	public function getFreightOneData(array $data, string $key);
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getMessageNotice()
	 */
	public function getMessageNotice(): array;
	/**
	 * 获取包邮地区关联字段
	 * 
	 * @return string
	 */
	public function getIdBySendSplitKey();
}