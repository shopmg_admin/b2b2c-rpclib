<?php

declare ( strict_types = 1 )
	;

namespace Lib\Freight;

use Lib\AbstractInterface;

interface FreightAreaInterface extends AbstractInterface {
	
	/**
	 * 获取运送地区
	 */
	public function getAddressArea(array $condition): array;
	/**
	 * 收货地址是否在包邮地区内
	 * 
	 * @return boolean
	 */
	public function sendAddressIsInFreeShipping(array $param): array;
	/**
	 * 获取关联字段
	 */
	public function getMialAreaSplitKey(): string;
}