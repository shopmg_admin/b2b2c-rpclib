<?php

declare ( strict_types = 1 )
	;

namespace Lib\Freight;

use Lib\AbstractInterface;

interface FreightModeInterface extends AbstractInterface {
	
	/**
	 * 获取运费配置
	 * 
	 * @return float
	 */
	public function getFreightMoney(array $data): array;
}