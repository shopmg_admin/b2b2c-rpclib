<?php
declare ( strict_types = 1 )
	;

namespace Lib;

interface AbstractInterface {
	
	/**
	 * 添加
	 *
	 * @param array $insert        	
	 * @return int
	 */
	public function addData(array $insert): int;
	
	/**
	 * 批量添加
	 * 
	 * @param array $post        	
	 * @return boolean
	 */
	public function addAll(array $post): int;
	/**
	 * 保存
	 *
	 * @param array $update        	
	 * @return int
	 */
	public function saveData(array $update): int;
	/**
	 * 删除
	 * 
	 * @param array $post        	
	 * @return int
	 */
	public function delete(array $post): int;
	/**
	 * 获取主键编号
	 */
	public function getPrimaryKey(): string;
	
	/**
	 * 获取错误信息
	 * @return string
	 */
	public function getErrorMessage(): string;
	
	/**
	 * 根据主表数据查从表数据
	 *
	 * @param array $data
	 * @param string $splitKey
	 * @return array
	 */
	public function getSlaveDataByMaster(array $data, string $splitKey): array;
	
	/**
	 * 获取分页数据列表
	 * @param array $post
	 * @param array $where
	 * @return array
	 */
	public function getParseDataByList(array $post, array $where = []): array;
	
	
	/**
	 * 获取分页数据列表 无搜索
	 *
	 * @param array $post
	 * @return array
	 */
	public function getParseDataByListNoSearch(array $post): array;
	
	/**
	 * 获取无分页数据列表
	 *
	 * @param array $post
	 * @return array
	 */
	public function getNoPageList(array $post): array;
}