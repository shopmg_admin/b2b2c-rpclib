<?php

declare ( strict_types = 1 )
	;

namespace Lib\CooperativePartner;

use Lib\AbstractInterface;

interface CooperativePartnerInterface extends AbstractInterface {
	
	/**
	 * 获取合作伙伴列表
	 * 
	 * @return array
	 */
	public function getCooprativePartner(): array;
	/**
	 * 获取合作伙伴列表并缓存
	 * 
	 * @return array
	 */
	public function getCooprativePartnerCache(): array;
}