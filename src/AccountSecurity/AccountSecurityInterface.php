<?php

declare ( strict_types = 1 )
	;

namespace Lib\AccountSecurity;

use Lib\AbstractInterface;

interface AccountSecurityInterface extends AbstractInterface {
	/**
	 *
	 * @return the $cache
	 */
	public function getCache();
	public function getValidateByBankCard();
	
	/**
	 *
	 * @name 账户安全 - 用户信息
	 */
	public function getUserInfo();
	public function verify(): string;
	/**
	 * 验证数据
	 */
	public function getValidateByEditMobile(): array;
	public function getShortMassageVerification(array $post): bool;
	public function getShortMassageVerif(array $post);
	/**
	 * 新密码
	 */
	public function newPassword(array $data): bool;
	public function addmailbox(array $post);
	public function checkEmail(array $post);
	public function checkEmailedit(array $data);
	/**
	 * 修改密码
	 */
	public function editUpdatePassword(array $data): bool;
	/**
	 * 验证手机
	 * 
	 * @return array
	 */
	public function getValidatePhone(): array;
}