<?php

declare ( strict_types = 1 )
	;

namespace Lib\Announcement;

use Lib\AbstractInterface;

interface AnnouncementInterface extends AbstractInterface {
	
	/**
	 * 得到所有的公告
	 */
	public function getAllAnnoucement(): array;
	/**
	 * 得到公告详情
	 */
	public function getAnnoucementInfo(array $post): array;
}