<?php

declare ( strict_types = 1 )
	;

namespace Lib\CashWithDrawal;

use Lib\AbstractInterface;

interface CashWithDrawalInterface extends AbstractInterface {
	
	/**
	 *
	 * @return array
	 */
	public function getSplitKeyByPay(): string;
	/**
	 *
	 * @return array
	 */
	public function getValidateByCash(): array;
	/**
	 * 添加提现日志
	 * 
	 * @param $money 余额        	
	 */
	public function addLog(float $money, array $data): bool;
	/**
	 * 提现行为 修改状态
	 * 
	 * @param array $result
	 *        	回调结果
	 */
	public function cashBehaviorEditStatus(array $payResult, array $data): bool;
	/**
	 * 获取提现信息
	 * 
	 * @return array
	 */
	public function getCashInfo(array $post): array;
}