<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreFollowInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateBySearch();
	public function getAttenByUser(array $post): int;
	
	
	/**
	 * 关注店铺
	 */
	public function attenStore(array $post): bool;
	/**
	 * 店铺取消关注
	 * 
	 * @return bool
	 */
	public function cancelAttenStore(array $post): bool;
	/**
	 * 我的收藏--店铺收藏删除
	 */
	public function getStoreFollowDel();
	/**
	 * 我的收藏--店铺收藏
	 */
	public function getMyCollectionStore(array $post): array;
	/**
	 * 获取店铺关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 获取店铺粉丝数量
	 * 
	 * @return int
	 */
	public function getFansNumber(array $data): int;
	/**
	 * 获取粉丝数量
	 * 
	 * @return int
	 */
	public function getAtten(array $post): int;
	/**
	 * 获取关注数据
	 * 
	 * @return array
	 */
	public function getDataByPerson(array $data): array;
	/**
	 * 是否关注
	 * 
	 * @return bool
	 */
	public function isConcerned(array $post): bool;
	/**
	 * 搜索获取数据
	 * 
	 * @param array $post        	
	 * @return array
	 */
	public function getStoreFollowSearch(array $post, array $accessWhere): array;
}