<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StorePersonInterface extends AbstractInterface {
	
	
	/**
	 * 编辑时
	 *
	 * @return array
	 */
	public function getValidateByLoginMergeAddess(): array;
	/**
	 * 个人开店
	 *
	 * @return boolean
	 */
	public function personEnter(array $data): int;
	/**
	 * 是否可以入住
	 */
	public function isCheckIn(): bool;
	/**
	 * 获取店铺信息
	 */
	public function getStoreInfo(): array;
	/**
	 * 获取店铺信息
	 *
	 * @return array
	 */
	public function getStore(): array;
	/**
	 * 修改状态
	 *
	 * @return bool
	 */
	public function editStatus(array $param): bool;
	/**
	 * 保存编辑基本开店信息
	 */
	public function saveEdit(array $data): bool;
}