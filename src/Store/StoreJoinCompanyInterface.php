<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreJoinCompanyInterface extends AbstractInterface {
	
	
	/**
	 * 编辑（未审核或者）
	 *
	 * @return array
	 */
	public function getValidateByApproval(): array;
	public function getValidateByBankInfo();
	
	
	
	/**
	 * 企业入驻信息提交
	 */
	public function addCompanyInfo(array $data): int;
	/**
	 * 是否可以入住
	 */
	public function isCheckIn(): bool;
	/**
	 * 获取店铺信息
	 */
	public function getStoreInfo(): array;
	/**
	 * 获取店铺信息
	 *
	 * @return array
	 */
	public function getStore();
	/**
	 * 修改状态
	 *
	 * @return bool
	 */
	public function editStatus(array $param);
	/**
	 * 保存编辑基本开店信息
	 */
	public function saveEdit(array $data): bool;
}