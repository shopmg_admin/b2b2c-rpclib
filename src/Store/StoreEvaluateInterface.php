<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreEvaluateInterface extends AbstractInterface {
	
	/**
	 * 获取店铺评分
	 */
	public function getStoreScore(array $post);
}