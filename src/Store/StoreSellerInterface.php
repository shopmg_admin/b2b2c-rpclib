<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreSellerInterface extends AbstractInterface {
	/**
	 * 获取插入的id
	 * 
	 * @return number
	 */
	public function getInstertId();
	/**
	 *
	 * @param field_type $otherWhere        	
	 */
	public function setAssociationWhere($otherWhere);
	
	/**
	 * 获取商家账号
	 * 
	 * @return array
	 */
	public function getSellerDataByStore(array $data, string $splitKey);
}