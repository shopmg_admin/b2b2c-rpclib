<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreBindInterface extends AbstractInterface {
	public function getClassIdByArray(array $data): array;
	/**
	 * 获取绑定的分类
	 *
	 * @return array
	 */
	public function getStoreBindClass(array $post): array;
	/**
	 * 验证店铺编号
	 *
	 * @return array
	 */
	public function getValidateStoreId(): array;
}