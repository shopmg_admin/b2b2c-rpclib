<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreClassInterface extends AbstractInterface {
	
	
	
	/**
	 * 获取店铺分类
	 * 
	 * @return mixed|boolean|string|NULL|array|unknown|object|array
	 */
	public function getStoreClass(): array;
	/**
	 * 根据店铺数据获店铺分类数据
	 * 
	 * @param array $storeData        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getStoreClassByStore(array $storeData, string $splitKey): array;
}