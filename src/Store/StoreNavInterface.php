<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreNavInterface extends AbstractInterface {
	public function getStoreNav(array $post): array;
	/**
	 * /获取店铺导航有缓存
	 * 
	 * @param array $post        	
	 * @return array
	 */
	public function getStoreNavCache(array $post): array;
}