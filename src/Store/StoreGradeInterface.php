<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreGradeInterface extends AbstractInterface {
	
	/**
	 * *
	 * 获取所有的店铺等级
	 */
	public function shopLevelList(): array;
	public function shopLevelListCache(): array;
	/**
	 * 获取店铺等级
	 */
	public function getStoreGrade(array $data): array;
}