<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreAdvInterface extends AbstractInterface {
	
	/**
	 * 获取广告
	 *
	 * @return array
	 */
	public function getAdByButton(array $post): array;
	/**
	 * 获取banner
	 */
	public function getBanner(array $post): array;
	/**
	 * 获取 不规则图片
	 */
	public function getBannerButton(array $post);
}