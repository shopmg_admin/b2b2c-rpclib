<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreMemberLevelInterface extends AbstractInterface {
	
	/**
	 *
	 * @return array
	 */
	public function parseMemberLevel(array $data, string $splitKey): array;
	/**
	 * 获取会员等级列表
	 *
	 * @return array
	 */
	public function getLevelList(array $data, string $splitKey): array;
}