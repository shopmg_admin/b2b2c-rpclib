<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreManagementCategoryInterface extends AbstractInterface {
	
	/**
	 *
	 * @return bool
	 */
	public function addStoreManagerment(array $data): bool;
	/**
	 * 获取 绑定分类
	 * 
	 * @return array
	 */
	public function getBindClassByStore(): array;
	/**
	 * 保存绑定信息
	 * 
	 * @return bool
	 */
	public function saveBindClass(array $class): bool;
}