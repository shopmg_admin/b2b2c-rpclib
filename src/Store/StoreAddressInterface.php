<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreAddressInterface extends AbstractInterface {
	
	/**
	 * 获取省份数据
	 * 
	 * @return mixed|NULL|unknown|string[]|unknown[]|object|array|mixed|NULL|unknown|string[]|unknown[]
	 */
	public function getStoreAdressById();
	/**
	 * 获取地区相关字段
	 */
	public function getSearchKeyByProv();
	/**
	 * 获取店铺地址
	 */
	public function getStoreAddressId(array $post): array;
	/**
	 * 获取店铺地址 并缓存
	 * 
	 * @author 王强
	 */
	public function getStoreAddressIdCache(array $post): array;
	/**
	 * 添加店铺地址
	 * 
	 * @return boolean
	 */
	public function addAddressStore(array $data): bool;
	/**
	 * 保存 店铺地址信息
	 */
	public function saveAddress(array $post): bool;
	/**
	 * 获取店铺关联条件
	 * 
	 * @return array
	 */
	public function getAssocConditionByStore(array $post): array;
	/**
	 * 获取地区id数组
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getAddressListCache(array $data, string $splitKey): array;
	public function getAddressList(array $ids): array;
	/**
	 * 获取地区id数组
	 * 
	 * @param array $ids        	
	 * @return array
	 */
	public function getAreaIds(array $area): array;
}