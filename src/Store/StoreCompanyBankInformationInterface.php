<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreCompanyBankInformationInterface extends AbstractInterface {
	
	
	
	/**
	 * 企业入驻--填写银行卡结算信息
	 */
	public function addBankInfo(array $data);
	/**
	 * 验证添加信息
	 */
	public function getMessageValidateBankInfo(): array;
	/**
	 * 获取银行卡信息
	 */
	public function getBankInformation(): array;
}