<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreInterface extends AbstractInterface {
	
	/**
	 *
	 * @return array
	 */
	public function getStoreInfo(array $data, string $splitKey): array;
	/**
	 * 根据其他数据获取店铺数据
	 * 
	 * @return array
	 */
	public function getStoreInforByOtherData(array $data, string $splitKey);
	/**
	 * 获取店铺数据
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getStoreByData(array $data, string $splitKey): array;
	/**
	 * 获取店铺数据有缓存
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getStoreByDataByCache(array $data, string $splitKey): array;
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\Specific\AbstractGetDataLogic::likeSerachArray()
	 */
	
	/**
	 * 获取店铺详情
	 */
	public function getShopDetails(array $post): array;
	public function calculationScore($data);
	public function calculationScoreAll($score, $scoreAll);
	/**
	 * 处理返回数据
	 */
	public function handelData($data);
	/**
	 * 获取店铺最新动态最近一个月的
	 */
	public function detaliTime($datas);
	/**
	 * 更新店铺销量
	 */
	public function updateSale(array $data);
	/**
	 *
	 * @param array $info        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getInfo(array $info, string $splitKey): array;
	/**
	 * 获取自营店铺
	 * 
	 * @return array
	 */
	public function getSelfStoreList(array $post): array;
	/**
	 * 获取自营店铺缓存
	 * 
	 * @return array
	 */
	public function getSelfStoreListCache(array $data): array;
	/**
	 * 店铺街换一换获取数据
	 */
	public function getShopStreetForAChange(): array;
	/**
	 * 店铺街换一换
	 */
	public function shopStreetForAChangeLogic(): array;
	public function getStoreByContrast(array $data, string $splitKey): array;
	/**
	 * 获取店铺信息
	 */
	public function getStoreInfoByStoreIdString(array $condition): array;
	/**
	 * 获取店铺信息
	 */
	public function getStoreInfoByStoreIdStringCache(array $data, string $splitKey): array;
	/**
	 * 【首页】搜索商品
	 */
	public function searchByIndexHome(array $post): array;
	/**
	 * 【首页】搜索商品
	 */
	public function getStoreList(array $param, array $assoccWhere): array;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\Specific\AbstractGetDataLogic::searchArray()
	 */
	public function searchArray(): array;
	/**
	 * 获取店铺分类关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByStoreClassId(): string;
	/**
	 * 店铺增加粉丝数
	 */
	public function storesIncreaseTheNumberOfFans(array $post): int;
	/**
	 * 店铺增加粉丝数（事物）
	 */
	public function storesIncreaseTheNumberOfFansByTrancestation(array $post): bool;
	/**
	 * 店铺减少粉丝数
	 */
	public function storesReduceTheNumberOfFans(array $post): int;
	/**
	 * 店铺减少粉丝数（事物）
	 */
	public function storesReduceTheNumberOfFansByTrancestation(array $post): bool;
}