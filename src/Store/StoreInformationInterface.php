<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreInformationInterface extends AbstractInterface {
	
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByBail();
	
	
	
	/**
	 * 公司信息
	 * 
	 * @return bool
	 */
	public function perfectCompanyInfo(array $post): bool;
	/**
	 * 获取店铺信息
	 */
	public function getStoreInfoByStore(array $data): array;
	/**
	 * 保存信息
	 * 
	 * @return bool
	 */
	public function saveInformation(array $post): bool;
	/**
	 * 获取店铺信息（审核）
	 */
	public function getStoreInfoByStoreByApproval(): array;
}