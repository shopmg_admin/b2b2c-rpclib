<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreMemberInterface extends AbstractInterface {
	
	/**
	 *
	 * @param array $result        	
	 */
	public function parseMember(array $result);
	/**
	 * 获取具体的店铺会员
	 *
	 * @return array
	 */
	public function getDetailMember(array $result);
	
	/**
	 * 当前登录用户是否是店铺会员缓存
	 */
	public function isMemberbByCurrentStore(): bool;
	/**
	 * 当前登录用户是否是店铺会员
	 */
	public function getMember(array $data);
}