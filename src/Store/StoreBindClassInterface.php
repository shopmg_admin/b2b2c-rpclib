<?php

declare ( strict_types = 1 )
	;

namespace Lib\Store;

use Lib\AbstractInterface;

interface StoreBindClassInterface extends AbstractInterface {
	
	
	
	/**
	 * 获取店铺分类
	 */
	public function getStoreClass(array $post): array;
}