<?php

declare ( strict_types = 1 )
	;

namespace Lib\Recharge;

use Lib\AbstractInterface;

interface RechargeInterface extends AbstractInterface {
	
	/**
	 * 添加余额数据
	 * 
	 * @return bool
	 */
	public function addRechargeData(array $data): array;
	/**
	 * 修改状态
	 * 
	 * @return boolean
	 */
	public function saveStatus(array $rechargeData): bool;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByPayType(): string;
}