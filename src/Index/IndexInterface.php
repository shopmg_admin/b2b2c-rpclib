<?php

declare ( strict_types = 1 )
	;

namespace Lib\Index;

use Lib\AbstractInterface;

interface IndexInterface extends AbstractInterface {
	
	/**
	 * 获取首页的信息
	 */
	public function getHomeInfo(): array;
}