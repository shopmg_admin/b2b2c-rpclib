<?php

declare ( strict_types = 1 )
	;

namespace Lib\Nav;

use Lib\AbstractInterface;

interface NavInterface extends AbstractInterface {
	
	/**
	 * 获取所有的分类模板
	 * 2017-12-19
	 */
	public function getWxNav();
	
	/**
	 * 获取首页导航菜单
	 */
	public function navList(): array;
}