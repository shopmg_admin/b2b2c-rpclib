<?php

declare ( strict_types = 1 )
	;

namespace Lib\HotWords;

use Lib\AbstractInterface;

interface HotWordsInterface extends AbstractInterface {
	
	
	
	/**
	 * 热门搜索
	 */
	public function hotWordSearch(): array;
	/**
	 * 热门搜索关键词带缓存
	 */
	public function hotWordSearchCache(): array;
}