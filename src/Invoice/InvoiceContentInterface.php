<?php

declare ( strict_types = 1 )
	;

namespace Lib\Invoice;

use Lib\AbstractInterface;

interface InvoiceContentInterface extends AbstractInterface {
	
	/**
	 * 发票内容数据
	 * 
	 * @return array
	 */
	public function invoiceContentData(): array;
	/**
	 * 发票内容缓存
	 * 
	 * @return array
	 */
	public function invoiceContentDataCache(): array;
}