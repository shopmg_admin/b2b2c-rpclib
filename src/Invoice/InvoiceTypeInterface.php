<?php

declare ( strict_types = 1 )
	;

namespace Lib\Invoice;

use Lib\AbstractInterface;

interface InvoiceTypeInterface extends AbstractInterface {
	public function getValidateByAddnvoices();
	public function getValidateBySaveIvoices();
	
	/**
	 * 发票类型数据
	 * 
	 * @return array
	 */
	public function invoiceList(): array;
	/**
	 * 发票类型缓存
	 * 
	 * @return array
	 */
	public function invoiceListCache(): array;
	/**
	 * 获取发票类型
	 * 
	 * @return array
	 */
	public function getInvoiceType(array $data, string $splitKey): array;
	/**
	 * 获取发票类型
	 * 
	 * @return array
	 */
	public function getInvoiceTypeCache(array $data, string $splitKey): array;
	/**
	 *
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function isOpenInvoice(array $data, string $splitKey): array;
}