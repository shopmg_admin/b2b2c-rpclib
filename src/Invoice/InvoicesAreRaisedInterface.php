<?php

declare ( strict_types = 1 )
	;

namespace Lib\Invoice;

use Lib\AbstractInterface;

interface InvoicesAreRaisedInterface extends AbstractInterface {
	public function getValidateByAddAreRaised(): array;
	/**
	 * 发票抬头数据
	 * 
	 * @return array
	 */
	public function invoiceAreRaiseData(): array;
	/**
	 * 发票抬头缓存
	 * 
	 * @return array
	 */
	public function invoiceAreRaiseDataCache(): array;
}