<?php

declare ( strict_types = 1 )
	;

namespace Lib\Invoice;

use Lib\AbstractInterface;

interface CapitaInvoiceInterface extends AbstractInterface {
	
	/**
	 * 多条数据获取地区id
	 *
	 * @param array $data        	
	 * @return array
	 */
	public function parseAreaWhere(array $data): array;
	/**
	 * 单条获取地区id条件
	 *
	 * @param array $data        	
	 * @return array
	 */
	public function parseAreaWhereOne(array $data): array;
	public function getCapitaDelete();
	public function getValidateByAddCapita(): array;
	public function getValidateByEditCapita(): array;
}