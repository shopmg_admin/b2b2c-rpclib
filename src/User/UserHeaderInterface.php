<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserHeaderInterface extends AbstractInterface {
	
	/**
	 * 验证头像
	 *
	 * @return array
	 */
	public function getValidateUserHeader(): array;
	/**
	 * 处理头像
	 *
	 * @return bool
	 */
	public function parseUserHeader(array $post): bool;
	
	
	/**
	 * 获取用户头像
	 * @return array
	 */
	public function getUserHeader(): array;
}