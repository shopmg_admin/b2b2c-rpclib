<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserDataInterface extends AbstractInterface {
	
	/**
	 * 积分
	 *
	 * @param array $integer        	
	 * @return bool
	 */
	public function updateIntegralByAdd(array $integer): bool;
	/**
	 * 根据用户获取积分
	 *
	 * @return array
	 */
	public function getIntegralByUserId(): array;
	public function getIntegralAndSaveSession(): array;
	/**
	 * 根据用户获取积分
	 *
	 * @return array
	 */
	public function getIntegralByUserIdCache(): array;
	/**
	 * 积分结算
	 */
	public function integralSettleMement(array $data): int;
	/**
	 * 积分关联字段
	 *
	 * @return string
	 */
	public function getSplitKeyByIntegral(): string;
}