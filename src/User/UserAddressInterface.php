<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserAddressInterface extends AbstractInterface {
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\Specific\AbstractGetDataLogic::likeSerachArray()
	 */
	
	/**
	 *
	 * @name 新增或者编辑收货地址验证规则
	 *      
	 *      
	 */
	public function getRuleByAddEditAddress();
	/**
	 *
	 * @name 添加收货地址
	 */
	public function addAddress(array $post): bool;
	/**
	 *
	 * @name 修改收货地址
	 *      
	 */
	public function editAddress(array $post);
	/**
	 *
	 * @name 收货地址列表验证规则
	 * @author 田利屏
	 */
	public function getRuleByAddressLists();
	public function getAddressLists(array $post): array;
	/**
	 *
	 * @name 设置默认收货地址
	 */
	public function setAddressDefault(array $post): bool;
	/**
	 *
	 * @name 查看默认收货地址
	 * @author 王强
	 */
	public function getDefaultAddress(): array;
	/**
	 * 获取地区ids
	 *
	 * @param array $areaData        	
	 * @return array
	 */
	public function getAreaIds(array $areaData): array;
	/**
	 * 获取地区ids(单条数据的)
	 *
	 * @param array $areaData        	
	 * @return array
	 */
	public function getAreaId(array $areaData): array;
	/**
	 * 获取收货地址
	 */
	public function getAddressByOrder(array $post, string $splitKey): array;
}