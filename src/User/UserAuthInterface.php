<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserAuthInterface extends AbstractInterface {
	/**
	 *
	 * @return boolean
	 */
	public function getIsLate();
	
	/**
	 * 检测是否已授权(wx)
	 */
	public function getWxData(array $data): array;
	
	/**
	 * 授权处理
	 */
	public function parseUserAuth(): int;
	/**
	 * 获取open id
	 */
	public function getOpenId(array $data): string;
}