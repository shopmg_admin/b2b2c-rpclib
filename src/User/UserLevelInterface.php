<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserLevelInterface extends AbstractInterface {
	
	/**
	 * 会员等级数据
	 */
	public function getList(): array;
	/**
	 * 会员等级数据
	 */
	public function getListCache(): array;
	/**
	 * 分析会员下一个等级
	 */
	public function parseUserNextLevel(array $integral, string $splitKey);
}