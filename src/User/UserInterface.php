<?php

declare ( strict_types = 1 )
	;

namespace Lib\User;

use Lib\AbstractInterface;

interface UserInterface extends AbstractInterface {
	/**
	 *
	 * @return the $cache
	 */
	public function getCache();
	
	
	
	/**
	 * 身份认证验证
	 *
	 * @return array
	 */
	public function getValidateByIdCard(): array;
	/**
	 * 身份认证
	 *
	 * @return bool
	 */
	public function authentication(array $data): bool;
	/**
	 * 用户登录逻辑
	 */
	public function userLogin(array $post): bool;
	/**
	 * 注册发送验证码验证规则
	 */
	public function getRuleByRegSendSms();
	/**
	 * 检测用户是否已经注册
	 */
	public function checkUserMobileIsExits(array $post): bool;
	/**
	 * 用户注册验证规则
	 *
	 * @author 王强
	 */
	public function getRuleByMobileRegister();
	/**
	 *
	 * 账户注册
	 *
	 * @author 王强
	 */
	public function userAccountRegister(array $post): array;
	/**
	 * 用户注册验证规则
	 */
	public function getRuleByAccountRegister();
	/**
	 * 用户短信登录-发送短信 验证规则
	 */
	public function getRuleBySendSmsLogin();
	/**
	 * 短信登录逻辑
	 */
	public function smsUserLogin(array $post): bool;
	/**
	 * 第三方登录验证规则
	 */
	public function getRuleByOtherLogin();
	/**
	 *
	 * @name 第三方账号绑定验证规则
	 *      
	 *       第三方账号绑定验证规则
	 *      
	 */
	public function getRuleBybindOther();
	/**
	 * 找回密码验证规则
	 */
	public function getRuleByBackPwd();
	/**
	 * 找回密码逻辑
	 */
	public function backUserPwd(array $data);
	/**
	 * 获取个人信息逻辑
	 */
	public function getUserInfo(): array;
	/**
	 * 修改密码验证规则
	 * 2017-12-21
	 */
	public function getRuleByModifyPassword();
	/**
	 * 第三方登录验证
	 */
	public function getMessageByBindAccount();
	/**
	 * 验证验证码
	 */
	public function checkPhoneNumber();
	/**
	 * 修改个人资料验证规则
	 */
	public function getRuleByEditUserInfo(): array;
	/**
	 *
	 * @return int
	 */
	public function editUserInfo(array $post): bool;
	/**
	 * 根据评论获取用户信息
	 *
	 * @param array $data        	
	 * @param string $splitKeys        	
	 * @return array|array
	 */
	public function getNameByComment(array $dataSources, string $splitKey);
	/**
	 * 获取用户昵称 并删除user_id
	 * 
	 * @param array $dataSources        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getNameByCommentByDeleteUserId(array $dataSources, string $splitKey): array;
	/**
	 * 授权登录
	 */
	public function addUserByQQ(array $post, array $serverParams): bool;
	/**
	 * 根据手机号获取用户信息
	 */
	public function getUserByMobile(array $post): array;
}