<?php

declare ( strict_types = 1 )
	;

namespace Lib\FootPrint;

use Lib\AbstractInterface;

interface FootPrintInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByDel();
	/**
	 * 返回验证数据
	 */
	public function getValidateBySearch();
	
	/**
	 * 最近浏览--删除
	 */
	public function getMyFootFrintDel(array $post): int;
	/**
	 * 浏览记录--搜索宝贝
	 */
	public function getMyFootFrintSearch(array $data);
	/**
	 * 个人中心
	 * 
	 * @return array
	 */
	public function guessYouLikeGoods(): array;
	/**
	 * 获取商品关联字段
	 * 
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 *
	 * @param unknown $limit        	
	 * @return array|unknown
	 */
	public function getMyLikeFootFrint(int $limit): array;
}