<?php

declare ( strict_types = 1 )
	;

namespace Lib\Feedback;

use Lib\AbstractInterface;

interface FeedbackInterface extends AbstractInterface {
	
	
	
	/**
	 *
	 * @name 用户反馈验证规则
	 *      
	 *       用户反馈验证规则
	 *       2017-12-22
	 */
	public function getRuleByFeedback();
	/**
	 *
	 * @name 用户登录逻辑
	 *      
	 *       用户登录逻辑
	 *       2017-12-20
	 */
	public function feedback();
}