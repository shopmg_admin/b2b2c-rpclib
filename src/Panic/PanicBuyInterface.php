<?php

declare ( strict_types = 1 )
	;

namespace Lib\Panic;

use Lib\AbstractInterface;

interface PanicBuyInterface extends AbstractInterface {
	/**
	 *
	 * @return the $cache
	 */
	public function getCache();
	
	/**
	 * 验证消息
	 * 
	 * @return array
	 */
	public function getVlidateMsg(): array;
	/**
	 * 获取抢购详情
	 * 
	 * @return array
	 */
	public function getPanicDetailByOrder(array $post): array;
	/**
	 * 获取抢购详情
	 * 
	 * @return array
	 */
	public function getPanicDetailByOrderCache(array $post): array;
	/**
	 * 购买之前返回数据
	 * 
	 * @return array
	 */
	public function beforeImmediatelyBuy(array $post): array;
	/**
	 * 获取商品分割key
	 * 
	 * @return string
	 */
	public function getGoodsBySplitKey(): string;
}