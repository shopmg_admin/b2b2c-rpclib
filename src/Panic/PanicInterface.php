<?php

declare ( strict_types = 1 )
	;

namespace Lib\Panic;

use Lib\AbstractInterface;

interface PanicInterface extends AbstractInterface {
	
	/**
	 * 获取商品关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 操作redis 添加库存
	 */
	public function redisOperation(array $goods, array $panic): void;
	/**
	 * 抢购内页
	 */
	public function innerPanic(array $post): array;
	/**
	 * 缓存
	 * 
	 * @param array $post        	
	 * @return array
	 */
	public function innerPanicCache(array $post): array;
	/**
	 * 获取本周热门抢购
	 * 
	 * @return array
	 */
	public function getHotBuyThisWeek(): array;
	/**
	 * 获取本周热门抢购（有缓存）
	 * 
	 * @return array
	 */
	public function getHotBuyThisWeekByCache(): array;
}