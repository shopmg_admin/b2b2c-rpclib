<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsPackageCartInterface extends AbstractInterface {
	
	
	public function getCartGoodsList(): array;
	public function getCartNumReduce(array $post);
	public function getCartNumModify(array $post): bool;
	/**
	 * 组合套餐加入购物车
	 */
	public function addPackageToCart(array $args, array $packageData, string $splitKey): bool;
	/**
	 * 获取套餐购物车信息用于购买
	 *
	 * @return array
	 */
	public function getPackageInfoByCart(array $post): array;
	/**
	 * 创建订单删除购物车
	 *
	 * @return bool
	 */
	public function deletePackageCart(): bool;
	/**
	 * 套餐关联字段
	 * 
	 * @return string
	 */
	public function getSplitKeyByPackage(): string;
	public function getSplitKeyByStore();
	/**
	 * 删除购物车
	 * 
	 * @param array $post        	
	 * @return bool
	 */
	public function toDelOrder(array $post): bool;
}