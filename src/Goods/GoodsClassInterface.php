<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsClassInterface extends AbstractInterface {
	
	
	public function getValidateByShop(): array;
	/**
	 * 获取所有的商品分类 获取所有的商品分类
	 */
	public function getAllClassees();
	
	/**
	 * 验证page
	 */
	public function getValidateByClassPage(): array;
	/**
	 * 分类必须是一推荐的
	 */
	public function getTopClass();
	/**
	 * 获取所有分类数据
	 * 
	 * @return
	 *
	 */
	public function getCacheByClassByPid(array $post): array;
	/**
	 * 分类必须是一推荐的
	 * 楼层数据
	 * 
	 * @return array
	 */
	public function getClassByPage(array $post);
	/**
	 * 根据一级分类获取二级分类
	 */
	public function getClassTwoByOne(array $oneClass): array;
	/**
	 * 获取三级分类
	 * 
	 * @return array
	 */
	public function getClassThreeByTwo(array $data): array;
	/**
	 *
	 * @param array $oneClass        	
	 * @return array
	 */
	public function getClassThreeByTwoCache(array $twoClass): array;
	/**
	 * 组装二三级分类
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function classTwoThreeBuild(array $data): array;
	/**
	 *
	 * @param array $oneClass        	
	 * @return array
	 */
	public function getClassTwoByOneCache(array $oneClass): array;
	/**
	 * 根据绑定的分类获取分类数据
	 */
	public function getGoodsClassByBindClass(array $classKey, array $bindClassData): array;
	/**
	 * 根据绑定的分类获取分类数据（未审核）
	 */
	public function getGoodsClassByBindClassByApproval(array $data): array;
	/**
	 * 获取分类信息
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function getGoodsClassByBindClassCache(array $data, array $cacheKey): array;
	/**
	 * 根据绑定的分类获取分类数据
	 */
	public function getGoodsClassByBindClassByIds(array $data): array;
	/**
	 *
	 * @param array $data        	
	 * @return array
	 */
	public function buildStoreGoodsClass(array $data): array;
}