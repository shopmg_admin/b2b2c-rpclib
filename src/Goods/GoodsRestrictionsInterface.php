<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsRestrictionsInterface extends AbstractInterface {
	
	
	/**
	 * 得到所有 的抢购商品包括首页广告图 商品图片 商品标题 活动抢购状态 抢购价格 原价 购买人数 设置提醒人数
	 */
	public function getAllRestrictGoods();
	
	
	
}