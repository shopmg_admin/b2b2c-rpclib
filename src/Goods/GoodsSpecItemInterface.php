<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsSpecItemInterface extends AbstractInterface {
	
	
	
	/**
	 * 获取规格项名称
	 * 
	 * @param array $data
	 *        	商品数组
	 * @param string $splitKey
	 *        	分割建
	 * @return array
	 */
	public function getSpecItemName(array $data, string $splitKey): array;
	/**
	 * 获取规格组相关字段
	 */
	public function getSplitKeyBySpecial();
}