<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsAttributeInterface extends AbstractInterface {
	
	/**
	 *
	 * @param array $data        	
	 * @param string $key        	
	 * @return array
	 */
	public function getGoodsAttribute(array $data, string $key): array;
	/**
	 * 获取商品对比列表
	 */
	public function goodsAttrList();
}