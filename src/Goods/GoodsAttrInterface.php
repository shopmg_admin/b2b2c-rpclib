<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsAttrInterface extends AbstractInterface {
	public function getGoodsAttrs(array $post): array;
	public function getSplitKeyByAtributeId(): string;
}