<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsCompareInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getMessageNotice();
	/**
	 * 商品对比
	 */
	public function compareGoods();
}