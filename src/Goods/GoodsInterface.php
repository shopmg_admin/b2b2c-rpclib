<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsInterface extends AbstractInterface {
	public function getCacheDriver();
	
	/**
	 * 获取 商品图片相关字段
	 */
	public function getImagesByGoods();
	
	/**
	 * 获取最新的商品
	 */
	public function getGoodsByNew();
	
	/**
	 * 验证信息
	 *
	 * @return array
	 */
	public function getValidateByContrast(): array;
	/**
	 * 返回验证数据
	 */
	public function getValidateBySearchGoods(): array;
	/**
	 * 获取子类数据
	 */
	public function getGoodsChildrenById(array $post): array;
	/**
	 * 获取商品父及id
	 */
	public function getGoodsChildrenByIdCache(array $post): int;
	/**
	 * 获取子类商品【上架的】
	 *
	 * @param int $pId
	 *        	商品父级编号
	 * @return array
	 */
	public function getChildrenGoods(array $post);
	/**
	 * 获取普通商品详情
	 */
	public function getGoodInfo(array $post): array;
	/**
	 * 获取全部商品中的一个详情
	 */
	public function getGoodInfoByOneOfTheTotalCommodities(array $post, string $splitKey): array;
	/**
	 * 获取全部商品中的一个详情缓存
	 */
	public function getGoodInfoByOneOfTheTotalCommoditiesCache(array $post, string $splitKey): array;
	/**
	 * 获取商品详情
	 */
	public function getGoodInfoCache(array $post): array;
	/**
	 * 根据商品收藏获取商品
	 */
	public function getGoodsByCollect(array $data, string $spiltKey): array;
	/**
	 * 根据获取的积分商品获取商品信息
	 */
	public function getGoodsDataByIntegral(array $data, string $splitKey): array;
	/**
	 * 根据获取的积分获取商品信息
	 */
	public function getGoodsDataByIntegralCache(array $args): array;
	/**
	 * 获取商品详情（无缓存）
	 *
	 * @return array
	 */
	public function getGoodsDetailById(array $param, string $splitKey): array;
	/**
	 * 获取商品详情（无缓存）
	 *
	 * @return array
	 */
	public function getPanicGoodsDetailById(array $post, string $splitKey): array;
	/**
	 * 商品详情
	 */
	public function getPanicGoodsDetailCacheKey(array $post, $splitKey);
	/**
	 * 商品详情
	 */
	public function getGoodsDetailCacheKey(array $post, string $splitKey): array;
	/**
	 * 获取商品详情
	 *
	 * @return array
	 */
	public function getGoodsDetailCache(array $param): array;
	public function getGoodsDetailByOrder(array $param): array;
	/**
	 * 获取自营店铺商品
	 *
	 * @return array
	 */
	public function getSelfStoreGoods(array $data): array;
	/**
	 * 根据店铺编号获取店铺全部商品列表
	 */
	public function getGoodsListByStore(array $post): array;
	/**
	 * 获取积分热兑商品
	 */
	public function getGoodsByIntegralHot(): array;
	/**
	 * 获取积分热兑商品
	 */
	public function getGoodsByIntegralHotCache(): array;
	/**
	 * 获取自营店铺商品
	 *
	 * @return array
	 */
	public function getSelfStoreGoodsCache(array $args, array $data): array;
	/**
	 * 获取品牌关联字段
	 *
	 * @return string
	 */
	public function getSplitKeyByBrand(): string;
	/**
	 * 获取关联字段
	 *
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 验证库存是否足够
	 */
	public function checkStock(array $stock);
	/**
	 * 获取库存
	 */
	public function getStock(array $data): array;
	/**
	 * 检测库存是否足够
	 */
	public function isThereEnoughStock(array $post): bool;
	/**
	 * 再次购买验证库存
	 */
	public function orderBuyAgainCheckStock(array $source): bool;
	/**
	 * 减少库存
	 */
	public function delStock(array $data): bool;
	/**
	 * 获取商品数据并缓存（购物车立即购买用）
	 *
	 * @return array
	 */
	public function getGoodsByOtherDataCache(array $dataByCart, string $splitKey): array;
	/**
	 * 根据其他数据获取商品数据（map）
	 */
	public function getGoodsByOtherData(array $data, string $splitKey);
	/**
	 * 根据数据获取商品
	 *
	 * @return array
	 */
	public function getGoodsByData(array $data, string $splitKey): array;
	/**
	 * 新品推荐(单个分类)
	 */
	public function newArrivals(array $post): array;
	/**
	 * 新品推荐(全部分类)
	 */
	public function newArrivalsAll(array $post): array;
	/**
	 * 推荐商品(根据店铺)
	 */
	public function recommendByStore(array $post): array;
	/**
	 * 新品上架(根据店铺)
	 */
	public function newArrivalsAllByStore(array $post): array;
	/**
	 * 店铺--爆品专区
	 */
	public function getDetonating(array $post): array;
	/**
	 * 热卖商品(具体分类中)
	 */
	public function hotSellingGoods(array $post): array;
	/**
	 * 热卖商品(全部分类中)
	 */
	public function hotSellingGoodsAll(array $post): array;
	/**
	 * 疯狂抢购
	 *
	 * @return array
	 */
	public function panicBuying(array $post): array;
	/**
	 * 猜你喜欢
	 *
	 * @return array
	 */
	public function guessWhatYouLike(array $post): array;
	/**
	 * 获取对比商品
	 */
	public function getContrastGoodsById(array $post): array;
	/**
	 * 得到商品的详情
	 */
	public function getGoodsInfoByGoodsId(array $source, string $splitKey): array;
	/**
	 * 分类关联key
	 *
	 * @return array
	 */
	public function getClassIdKey(): array;
	/**
	 * 获取 商品数据
	 *
	 * @return array
	 */
	public function getGoodsForData(array $data, string $splitKey): array;
	/**
	 * 套餐相关
	 *
	 * @return array
	 */
	public function getParseDataByOrder(array $data, string $splitKey): array;
	/**
	 * 套餐购物车购买兼容支付数据
	 *
	 * @return array
	 */
	public function getParseDataCartByOrder(array $data, string $splitKey): array;
	/**
	 * 【首页】搜索商品
	 */
	public function searchByIndexHome(array $post): array;
	/**
	 * 店内搜索验证
	 *
	 * @return array
	 */
	public function getValidateByShopSearch(): array;
	/**
	 * 根据店铺信息获取商品信息
	 */
	public function getGoodsListByStoreInfo(array $store): array;
	/**
	 * 验证 库存是否足够
	 */
	public function getGoodsByIdString(array $data): array;
	/**
	 * 查看库存是否足够
	 *
	 * @return array
	 */
	public function checkGoodsStock(array $data): array;
	/**
	 * 最佳配件商品立即购买缓存
	 */
	public function bestAccessoriesImmediatePurchaseCache(array $post): array;
	/**
	 * 最佳配件商品立即购买
	 */
	public function bestAccessoriesImmediatePurchase(array $post): array;
	/**
	 * 获取组合商品
	 *
	 * @param array $data        	
	 * @return array
	 */
	public function getGoodsCombo(array $condition): array;
	/**
	 * 验证店内搜索
	 *
	 * @return array
	 */
	public function vlidateParam(): array;
	/**
	 * 获取店铺商品数量
	 */
	public function getGoodsNumberByStore(array $data): int;
	/**
	 * 获取店铺商品数量(有缓存)
	 */
	public function getGoodsNumberByStoreCache(array $data): int;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByPid(): string;
}