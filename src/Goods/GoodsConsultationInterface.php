<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsConsultationInterface extends AbstractInterface {
	
	/**
	 * 获取一个商品的咨询
	 */
	public function getGoodsConsultationByGoods(array $data): array;
	/**
	 * 验证提问
	 */
	public function getMessageValidate(): array;
	
	/**
	 * 管理员添加回复
	 */
	public function addContent(array $post);
	/**
	 * 获取当前商品的咨询
	 * 
	 * @param numeric $id
	 *        	商品编号
	 * @return array 咨询数据数组
	 */
	public function getConsulation($id, $number);
	/**
	 * 提交咨询
	 */
	public function addConsulation(array $post);
	/**
	 * 删除问题及其回答
	 */
	public function deleteAllConsulationById($id);
	/**
	 * 获取商品关联字段
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 获取商品关联字段
	 */
	public function getSplitKeyByUser(): string;
}