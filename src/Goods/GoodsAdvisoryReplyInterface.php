<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsAdvisoryReplyInterface extends AbstractInterface {
	
	/**
	 * 获取每个回答中的一条
	 * SELECT replay.user_id, replay.id, replay.content, replay.consultation_id, replay.type
	 * FROM (
	 * SELECT MAX(id) as id FROM mg_goods_advisory_reply
	 * WHERE consultation_id in (1, 2)
	 * GROUP BY consultation_id
	 * ) goods_adv INNER JOIN mg_goods_advisory_reply as replay
	 * ON goods_adv.id = replay.id
	 */
	public function getGoodsAdvisoryReply(array $data, string $splitKey);
	/**
	 * 获取每个回答中的一条并缓存
	 */
	public function getParseGoodsAdvisoryReply(array $data, string $splitKey);
}