<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsItemInterface extends AbstractInterface {
	
	/**
	 * 获取店内排行商品
	 *
	 * @return array
	 */
	public function getGoodsByStoreRankings(array $post): array;
	/**
	 * 获取店内排行商品
	 *
	 * @return array
	 */
	public function getGoodsByStoreRankingsCache(array $post): array;
	/**
	 * 获取验证方法
	 *
	 * @return array
	 */
	public function getValidateStoreId(): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByPid(): string;
	/**
	 * 商品热词搜索验证
	 */
	public function getvalidateByHotSearch(): array;
	/**
	 *
	 * @return array
	 */
	public function getHotSearchList(array $post): array;
}