<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsSpecInterface extends AbstractInterface {
	
	
	
	
	
	/**
	 * 获取商品规格组名称
	 * 
	 * @author 王强
	 */
	public function getGoodSpecial(array $data, string $splitKey): array;
}