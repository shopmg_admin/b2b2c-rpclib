<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsAccessoriesInterface extends AbstractInterface {
	
	
	
	/**
	 * 得到互补商品
	 */
	public function getComplementaryCommodities(array $post): array;
}