<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsDetailInterface extends AbstractInterface {
	
	
	
	/**
	 * 获取商品详情
	 */
	public function getGoodDetail(array $post): string;
	/**
	 * 验证字段
	 */
	public function getMessageByDetail();
}