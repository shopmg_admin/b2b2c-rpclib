<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsImagesInterface extends AbstractInterface {
	
	/**
	 *
	 * @return array
	 */
	public function getSlaveField();
	/**
	 * 检查参数
	 */
	public function checkValidateByGoodsId();
	/**
	 * 获取压缩的图片
	 */
	public function getThumbImagesByGoodsId(array $goods, string $splitKey): array;
	/**
	 * 获取自营店铺商品
	 *
	 * @return array
	 */
	public function getSelfStoreGoodsImage(array $data, string $splitKey): array;
	/**
	 * 获取自营店铺商品
	 *
	 * @return array
	 */
	public function getSelfStoreGoodsImageCache(array $args, array $goods, string $splitKey): array;
	/**
	 * 获取图片（根据商品）
	 */
	public function getImageByArrayGoods(array $source): array;
	/**
	 * 获取商品图片(商品详情)
	 */
	public function getGoodsImagesByPid(array $data, string $splitKey): array;
	/**
	 * 获取商品图片(商品详情)
	 */
	public function getGoodsImagesByPidCache(array $data, string $splitKey): array;
}