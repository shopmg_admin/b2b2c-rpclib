<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface SpecGoodsPriceInterface extends AbstractInterface {
	
	/**
	 * 获取商品规格数据
	 * 
	 * @return array
	 */
	public function getSpecByGoods(array $data, string $splitKey): array;
	/**
	 * 获取 规格相关字段
	 */
	public function getSplitKeyByGoods();
}