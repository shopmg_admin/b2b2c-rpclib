<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsPackageInterface extends AbstractInterface {
	/**
	 *
	 * @return the $cache
	 */
	public function getCache();
	
	/**
	 * 套餐购物车检查数据
	 * 
	 * @return array
	 */
	public function getValidateByGoodsPackage(): array;
	public function getPackageBuyNow(array $post): array;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getMessageNotice()
	 */
	public function getMessageNotice(): array;
	/**
	 * 获取商品套餐
	 */
	public function getPackageListCache(array $post): array;
	/**
	 * 获取商品套餐
	 */
	public function getPackageList(array $post): array;
	/**
	 * 获取关联字段
	 * 
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 根据编号获取套餐数据
	 */
	public function getPackageListByIds(array $post): array;
	/**
	 * 根据套餐购物车获取套餐商品数据
	 * 
	 * @return array
	 */
	public function getPackageByPackageCart(array $dataSources, string $splitKey): array;
}