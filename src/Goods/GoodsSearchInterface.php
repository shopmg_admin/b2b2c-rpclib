<?php
declare ( strict_types = 1 )
	;

namespace Lib\Goods;

/**
 * 商品搜索
 * 
 * @author mg
 *        
 */
interface GoodsSearchInterface {
	/**
	 * 商品列表
	 * 
	 * @return array
	 */
	public function getGoodsList(array $condition, array $post): array;
}