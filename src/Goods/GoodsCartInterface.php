<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsCartInterface extends AbstractInterface {
	/**
	 * 获取缓存管理
	 *
	 * @return \Swoft\Cache\Cache
	 */
	public function getCacheManager();
	
	/**
	 * 返回验证数据
	 */
	public function getMessageNotice();
	public function getCartIdValidate();
	
	
	
	public function getConfigs();
	public function getGoodsCount();
	/**
	 * 获取购物车列表
	 */
	public function getCartGoodsList(array $post): array;
	/**
	 * 获取购物车列表
	 */
	public function getCartGoodsListByUser();
	/**
	 * 商品加入购物车
	 *
	 * @author 王强
	 */
	public function addCart(array $goodsData): bool;
	/**
	 * 删除购物车商品
	 */
	public function delGoodsCart(array $data): int;
	/**
	 * 单个购物车移入收藏夹 删除购物车
	 *
	 * @param array $post        	
	 * @return bool
	 */
	public function delGoodsCartOneByTrancestation(array $post): bool;
	/**
	 * 多个购物车移入收藏夹删除购物车商品
	 */
	public function delManyGoodsCartsTrancsation(array $data): bool;
	/**
	 * 删除购物车商品
	 */
	public function delManyGoodsCarts(array $data);
	public function getCartNumModify(array $post);
	/**
	 * 删除购物车数据(购物车生成订单时)
	 */
	public function deleteCartByTrans(array $cartId): bool;
	/**
	 * 删除购物车(购物车生成订单时)
	 *
	 * @return boolean
	 */
	public function deleteCart(array $cartId);
	/**
	 * 获取购物车信息并缓存（用于购物车生成订单0）
	 */
	public function getGoodsCartInfoCache(array $post): array;
	/**
	 * 用于购物车生成订单
	 */
	public function getGoodsCartInfo(array $post): array;
	public function getUserBuyCarGoodsInfo(): array;
	/**
	 * 获取关联字段
	 *
	 * @return string
	 */
	public function getSplitKeyByGoodsId(): string;
	/**
	 * 获取关联字段
	 *
	 * @return string
	 */
	public function getSplitKeyByStoreId(): string;
	/**
	 * 多个商品加入购物车
	 *
	 * @author 王强
	 */
	public function addCarAll(array $data, string $splitKey): bool;
	/**
	 * 根据商品和用户获取购物车数据
	 */
	public function getCartByGoodsIdAndUserId(array $data, string $splitKey): array;
}