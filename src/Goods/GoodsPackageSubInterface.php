<?php

declare ( strict_types = 1 )
	;

namespace Lib\Goods;

use Lib\AbstractInterface;

interface GoodsPackageSubInterface extends AbstractInterface {
	
	/**
	 * 获取套餐数据
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getPackageSub(array $data, string $splitKey): array;
	/**
	 * 获取套餐商品
	 * 
	 * @return array
	 */
	public function getGoodsPackageSubListByPackageCache(array $packageGoods, string $splitKey): array;
	/**
	 * 处理商品是否在套餐内
	 * 
	 * @return array
	 */
	public function parseGoodsIsInPackage(array $packageGoods, array $post, string $splitKey): array;
	/**
	 * 获取套餐商品
	 * 
	 * @return array
	 */
	public function getGoodsPackageSubListByPackage(array $packageIds): array;
	/**
	 * 获取商品套餐信息
	 * 
	 * @return array
	 */
	public function getGoodsPackageSub(): array;
	/**
	 * 获取套餐数据
	 */
	public function getPackageSubInfoByOrderPackage();
	
	/**
	 * 根据购物车数据获取套餐数据
	 * 
	 * @return array
	 */
	public function getGoodsPackageSubDataByGoodsCart(array $data, string $splitKey): array;
	public function getSplitKeyByGoods(): string;
}