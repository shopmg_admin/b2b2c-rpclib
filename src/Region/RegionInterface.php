<?php

declare ( strict_types = 1 )
	;

namespace Lib\Region;

use Lib\AbstractInterface;

interface RegionInterface extends AbstractInterface {
	
	
	
	/**
	 * 城市选择验证规则
	 */
	public function getRuleByRegionLists();
	/**
	 * 城市选择列表逻辑
	 */
	public function regionLists(array $post): array;
	/**
	 * 获取编辑地区数据
	 */
	public function getAddressDataByCSpecial(array $area): array;
	/**
	 * 获取地址缓存
	 *
	 * @return array
	 */
	public function getAddressDataCache(array $area): array;
	/**
	 *
	 * @return array
	 */
	public function getRegionByManyArea(array $area, array $ids): array;
	/**
	 * 根据增值税发票获取地区数据
	 *
	 * @return array
	 */
	public function getRegionByCapita(array $area, array $ids): array;
	/**
	 * 根据单条增值税发票获取地区数据
	 *
	 * @return array
	 */
	public function getRegionByOneCapita(array $area, array $ids): array;
}