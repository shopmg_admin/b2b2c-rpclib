<?php

declare ( strict_types = 1 )
	;

namespace Lib\Article;

use Lib\AbstractInterface;

interface ArticleCategoryInterface extends AbstractInterface {
	
	/**
	 *
	 * @name 查看文章分类列表
	 *      
	 *       查看文章分类列表
	 *       2018-01-05 12:18
	 */
	public function categoryLists(): array;
	/**
	 * 获取底部文章并缓存
	 */
	public function getCategoryListsCache(): array;
}