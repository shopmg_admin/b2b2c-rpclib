<?php

declare ( strict_types = 1 )
	;

namespace Lib\Article;

use Lib\AbstractInterface;

interface ArticleInterface extends AbstractInterface {
	
	
	
	/**
	 *
	 * @name 文章列表
	 *      
	 *       文章列表
	 *       2018-01-05 12:18
	 */
	public function lists();
	/**
	 * 文章详情
	 * 
	 * @return array|\Swoft\Core\ResultInterface
	 * @throws \Swoft\Db\Exception\DbException
	 */
	public function info(array $post);
	/**
	 * 获取文章列表
	 *
	 * @return array
	 */
	public function getArticleList(array $data, string $splitKey): array;
	/**
	 * 获取文章列表
	 *
	 * @return array
	 */
	public function getArticleListCache(array $data, string $splitKey): array;
	/**
	 * 获取分类下的文章
	 *
	 * @return array
	 */
	public function getArticleListByCategoryId(array $post): array;
	/**
	 * 获取分类下的文章
	 *
	 * @return array
	 */
	public function getArticleListByCategoryIdCache(array $post): array;
	/**
	 * 获取 最新文章
	 */
	public function lastestArticle(): array;
	/**
	 * 获取 最新文章并缓存
	 *
	 * @return array
	 */
	public function lastestArticleCache(): array;
}