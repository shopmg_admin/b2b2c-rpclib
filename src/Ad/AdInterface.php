<?php

declare ( strict_types = 1 )
	;

namespace Lib\Ad;

use Lib\AbstractInterface;

interface AdInterface extends AbstractInterface {
	
	/**
	 * 首页广告
	 * 
	 * @return array
	 */
	public function getAdByIndex(array $post): array;
	public function getSplitKeyByAdSpace();
	/**
	 * 楼层底部广告
	 *
	 * @return array
	 */
	public function floorAdvertising(array $post): array;
	/**
	 * 楼层底部广告
	 *
	 * @return array
	 */
	public function floorAdvertisingCache(array $post): array;
	/**
	 * 楼层中间广告
	 *
	 * @return array
	 */
	public function floorAdvertisement(array $param): array;
	/**
	 * 楼层中间广告
	 *
	 * @return array
	 */
	public function floorAdvertisementCache(array $param): array;
	/**
	 * 获取抢购广告list
	 *
	 * @return array
	 */
	public function getPanicAd(): array;
	/**
	 * 获取抢购广告list
	 *
	 * @return array
	 */
	public function getPanicAdByCache(): array;
	/**
	 * 获取积分抢购广告
	 * 
	 * @return array
	 */
	public function getIntegralShopCityAd(): array;
	/**
	 * 获取积分广告list
	 *
	 * @return array
	 */
	public function getIntegralShopCityAdCache(): array;
}