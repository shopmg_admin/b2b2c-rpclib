<?php

declare ( strict_types = 1 )
	;

namespace Lib\Complain;

use Lib\AbstractInterface;

interface ComplainInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByComplain();
	
	/**
	 * 获取 商品相关字段
	 */
	public function getSplitKeyByGoods();
	
	
	
	/**
	 * 我的订单--投诉申请
	 * 
	 * @param array $insert        	
	 * @return array
	 */
	public function getParseResultByAdd(array $insert): array;
	public function getSplitKeyByOrderId(): string;
	public function getSplitKeyByAccusedId(): string;
}