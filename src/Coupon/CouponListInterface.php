<?php

declare ( strict_types = 1 )
	;

namespace Lib\Coupon;

use Lib\AbstractInterface;

interface CouponListInterface extends AbstractInterface {
	
	
	
	/**
	 * 优惠券列表
	 * 
	 * @return number[]|string[]|number[]|string[]|string[][]|unknown[][]|number[][]
	 */
	public function myCouponLists(): array;
	/**
	 * 用户可用代金券
	 * 
	 * @return array
	 */
	public function getUsersCanUseCoupons();
	/**
	 * 获取优惠券相关字段
	 */
	public function getSplitKeyByCoupon(): string;
	/**
	 * 优惠券 处理
	 */
	public function couponParse(array $payData);
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getParseResultByAdd()
	 */
	public function getParseResultByAdd(array $insert): array;
}