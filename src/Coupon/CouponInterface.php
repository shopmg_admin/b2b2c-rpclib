<?php

declare ( strict_types = 1 )
	;

namespace Lib\Coupon;

use Lib\AbstractInterface;

interface CouponInterface extends AbstractInterface {
	
	/**
	 * 获取优惠券
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function getCoupon(array $data, string $splitKey): array;
	
	/**
	 * 优惠券是否可用
	 * 
	 * @return boolean
	 */
	public function checkCouponIsUse(array $post): bool;
	/**
	 * 获取该店铺优惠券
	 * 
	 * @param bool $isMemberByCurrentShop
	 *        	是否是当前店铺会员
	 */
	public function getCouponListByStore(bool $isMemberByCurrentShop, array $post): array;
	/**
	 * 获取该店铺优惠券
	 * 
	 * @param bool $isMemberByCurrentShop
	 *        	是否是当前店铺会员
	 */
	public function getCouponListByStoreCache(bool $isMemberByCurrentShop, array $post): array;
	/**
	 * 获取优惠券数据
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getCouponByData(array $dataSources, string $splitKey): array;
	/**
	 * 店铺关联字段
	 * 
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
}