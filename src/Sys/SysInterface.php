<?php

declare ( strict_types = 1 )
	;

namespace Lib\Sys;

use Lib\AbstractInterface;

interface SysInterface extends AbstractInterface {
	public function getAllConfig();
	/**
	 * 获取无缓存组配置
	 * 
	 * @return mixed|NULL|unknown|string[]|unknown[]|object
	 */
	public function getNoCacheConfigByGroup(string $key);
	/**
	 * 获取无缓存具体配置
	 * 
	 * @return mixed|NULL|unknown|string[]|unknown[]|object
	 */
	public function getDetailCacheConfig(string $key): array;
	/**
	 * c 获取具体的有缓存的配置
	 * 
	 * @return array
	 */
	public function getConfigByDetailKey(string $key): string;
	/**
	 * 转换配置(有缓存)
	 */
	public function covertMapByNoCacheConfig();
	/**
	 * c 依据某个键 获取 子集
	 * 
	 * @return array
	 */
	public function getDataByKey(string $key): array;
	/**
	 * 转换配置(有缓存)
	 */
	public function covertMapByConfig(string $key): array;
	/**
	 * 获取配置值
	 * 
	 * @return array
	 */
	public function getValue();
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\Specific\AbstractGetDataLogic::getMessageNotice()
	 */
	public function getMessageNotice(): array;
}