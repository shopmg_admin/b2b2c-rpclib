<?php

declare ( strict_types = 1 )
	;

namespace Lib\Balance;

use Lib\AbstractInterface;

interface BalanceInterface extends AbstractInterface {
	
	/**
	 * 获取验证数据
	 *
	 * @return string[][]
	 */
	public function getValidateByBankCard(): array;
	
	/**
	 * 获取余额
	 *
	 * @return float
	 */
	public function getBalanceMoney(): float;

	/**
	 * 提现
	 *
	 * @return bool
	 */
	public function cashDelMoney(array $data): bool;
	
	/**
	 *
	 * @name 我的账户-收入
	 *      
	 */
	public function getDetailsIncome(array $post): array;
	/**
	 *
	 * @name 我的账户-支出
	 */
	public function getDetailsPay(array $post): array;
	/**
	 * 余额充值
	 *
	 * @param array $recharge        	
	 * @param string $className        	
	 */
	public function rechargeMoney(array $recharge): bool;
	  /**
     * 开店
     *
     * @return array
     */
    public function openShopParse(array $data, string $description): array;
	
	/**
	 * 商品支付
	 * @param array $data
	 * @param string $description
	 * @return array
	 */
	public function rechargePay(array $data, string $description): array;
	
	/**
	 * 获取余额
	 *
	 * @return float
	 */
	public function getBalance();
	public function rollBack(): bool;
}