<?php

declare ( strict_types = 1 )
	;

namespace Lib\Express;

use Lib\AbstractInterface;

interface ExpressInterface extends AbstractInterface {
	
	/**
	 * 获取快递公司信息
	 * 
	 * @return array
	 */
	public function getExpressData(array $data, string $splitKey): array;
	/**
	 * 获取快递公司信息
	 * 
	 * @return array
	 */
	public function getExpressDataCache(array $data, string $splitKey): array;
	/**
	 * 获取快递列表
	 * 
	 * @return array
	 */
	public function getExpressDataList(): array;
	/**
	 * 获取快递列表并缓存
	 * 
	 * @return array
	 */
	public function getExpressDataListCache(): array;
	/**
	 * 是否已发货
	 * 
	 * @return array
	 */
	public function isDeliverGoods(array $data, string $splitKey): array;
}