<?php

declare ( strict_types = 1 )
	;

namespace Lib\Brand;

use Lib\AbstractInterface;

interface BrandInterface extends AbstractInterface {
	
	/**
	 * 获取推荐的品牌
	 * 
	 * @return array
	 */
	public function getRecommendBrandListCache(): array;
	
	/**
	 * 品牌列表逻辑
	 */
	public function getBrandLists(array $post): array;
	/**
	 * *分类详情
	 */
	public function getBrandCategory();
	/**
	 * 商品所在地
	 */
	public function goodslocation($message);
	/**
	 * 品牌店导航
	 */
	public function Brandnav();
	/**
	 * 获取品牌数据
	 * 
	 * @return array
	 */
	public function getBrandForGoods(array $data, string $splitKey): array;
	/**
	 * 获取品牌详情
	 */
	public function getDetail(array $post): array;
	public function getDetailCache(array $post): array;
}