<?php

declare ( strict_types = 1 )
	;

namespace Lib\Guess;

use Lib\AbstractInterface;

interface GuessInterface extends AbstractInterface {
	
	/**
	 * 根据评分获取商品
	 */
	public function getGoodsByScore(array $post, array $goods): array;
	/**
	 * 获取推荐商品并缓存
	 */
	public function getGoodsByScoreCache(array $post, array $data): array;
	/**
	 * 获取猜你喜欢商品（未登录时猜你喜欢）
	 *
	 * @return array
	 */
	public function getGuessLikeGoods(array $post, array $cookie): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByPid(): string;
}