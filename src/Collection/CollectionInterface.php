<?php

declare ( strict_types = 1 )
	;

namespace Lib\Collection;

use Lib\AbstractInterface;

interface CollectionInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidate(): array;
	
	/**
	 * 获取 商品相关字段
	 */
	public function getSplitKeyByGoods(): string;
	
	
	
	/**
	 * 购物车添加商品收藏
	 */
	public function collectGoods(array $post): bool;
	/**
	 * 购物车批量收藏
	 */
	public function batchCollectGoods(array $post): bool;
	/**
	 * 取消我收藏的商品
	 */
	public function cancelUserCollection(array $post): int;
	/**
	 * 我的收藏--商品
	 */
	public function getMyCollectionGoods();
	/**
	 * 根据搜索条件获取数据
	 * 
	 * @param array $post        	
	 * @param array $where        	
	 * @return array
	 */
	public function getCollectionBySearch(array $post, array $where): array;
	/**
	 * 收藏商品
	 */
	public function getCollectGoods(array $post): bool;
	/**
	 * 我的收藏--删除
	 */
	public function getMyCollectionDel(array $post): int;
	/**
	 * 删除检测是否是数字字符串
	 *
	 * @return array
	 */
	public function getValidateByDel(): array;
	/**
	 *
	 * @return array
	 */
	public function checkAlreadyCollect(array $goods): array;
}