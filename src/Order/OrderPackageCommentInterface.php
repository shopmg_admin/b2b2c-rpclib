<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageCommentInterface extends AbstractInterface {
	
	/**
	 * 验证评论数据
	 * 
	 * @return array
	 */
	public function validateDataByComment(): array;
	/**
	 * 添加评价
	 * 
	 * @return bool
	 */
	public function recordConmment(array $data): int;
}