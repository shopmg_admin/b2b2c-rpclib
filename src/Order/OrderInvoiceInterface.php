<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderInvoiceInterface extends AbstractInterface {
	
	/**
	 * 发票更新
	 */
	public function updateInvoice(array $invoiceData): bool;
	/**
	 * 发票->立即购买更新
	 */
	public function updateInvoiceByPlaceTheOrder(array $invoiceData): bool;
	/**
	 *
	 * @return array
	 */
	public function getMessageByAddInvoice(): array;
	/**
	 * 获取发票信息
	 * 
	 * @return string
	 */
	public function getInvoice(array $data): array;
	/**
	 * 获取发票信息
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function isOpenInvoice(array $data): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByType(): string;
}