<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageReviewProgressInterface extends AbstractInterface {
	
	/**
	 * 添加审核进度
	 *
	 * @return bool
	 */
	public function addOrderReview(array $post): bool;
}