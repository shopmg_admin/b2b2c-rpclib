<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageExchangeProgressInterface extends AbstractInterface {
	
	/**
	 * 添加审核进度
	 *
	 * @return bool
	 */
	public function addOrderReview(array $data): bool;
	/**
	 * 申请售后进度查询
	 * 
	 * @return array
	 */
	public function getProgressQuery(): array;
}