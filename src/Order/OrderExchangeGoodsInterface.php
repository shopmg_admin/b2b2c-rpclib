<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderExchangeGoodsInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByNumber(): array;
	public function setCourierNumber(): bool;
	/**
	 * 获取店铺关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByStoresId(): string;
	/**
	 * 获取订单关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByOrderId(): string;
	/**
	 * 获取订单关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByGoodsId(): string;
	/**
	 * 退换货处理
	 * 
	 * @return bool
	 */
	public function applyForAfterSale(array $post): int;
	/**
	 * 返回验证数据
	 */
	public function getValidateByOrderAfterSale(): array;
}