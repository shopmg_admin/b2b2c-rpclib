<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OpenShopOrderInterface extends AbstractInterface {
	
	/**
	 *
	 * @param array $data        	
	 * @return string
	 */
	public function pushOpenShopOrder(array $data): array;
	/**
	 * 修改状态
	 *
	 * @return boolean
	 */
	public function saveStatus(array $data): bool;
	/**
	 * 获取支付状态
	 *
	 * @return int
	 */
	public function getPayStatusById(array $post): int;
	/**
	 * 检查是否已支付
	 *
	 * @return bool
	 */
	public function isCheckPay(array $post): bool;
}