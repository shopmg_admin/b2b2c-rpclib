<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderIntegralInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByBuildOrder(): array;
	
	/**
	 * 积分兑换-立即兑换 参数验证
	 */
	public function getValidateByOrder();
	public function getValidateByOrderId();
	
	/**
	 * 积分兑换处理
	 */
	public function commitConfirm(array $post): array;
	/**
	 * 支付回调成功后 修改订单状态
	 */
	public function paySuccessEditStatus(array $orderId, array $payConfig);
	/**
	 * 取消订单
	 *
	 * @return bool
	 */
	public function cancelOrder(array $data): bool;
	/**
	 * 删除订单
	 *
	 * @return bool
	 */
	public function delOrder(array $data): bool;
	/**
	 * 根据订单编号获取订单信息
	 *
	 * @return array
	 */
	public function getOrderDetail(array $post): array;
	/**
	 * 地址关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByAddress(): string;
	/**
	 * 支付类型关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByPayType(): string;
	/**
	 * 快递关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByExp(): string;
	/**
	 * 快递关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 组装支付数据
	 *
	 * @return array
	 */
	public function orderPayBuildData(array $data): bool;
	/**
	 * 已签收
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getOverTime(array $post): bool;
	public function getOrder(array $post): array;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getSearchOrderKey()
	 */
	public function getSearchOrderKey(): array;
	/**
	 * 检查是否已支付
	 */
	public function isCheckPay(array $post): bool;
	/**
	 * 查询订单状态
	 * 
	 * @return array
	 */
	public function selectorderStatus(array $post): array;
	/**
	 * 检查数据
	 * 
	 * @return array
	 */
	public function getValidateByOrderCheck(): array;
}