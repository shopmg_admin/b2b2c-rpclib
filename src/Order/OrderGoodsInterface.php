<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderGoodsInterface extends AbstractInterface {
	/**
	 *
	 * @return string
	 */
	public function getCartId(): array;
	
	/**
	 * 确认收货
	 */
	public function confirmGetGoods(array $post): bool;
	
	/**
	 * 统计店铺销量最高的商品
	 */
	public function statisticalGoodsTop(array $data, string $splitKey);
	/**
	 * 获取商品相关字段
	 */
	public function getSplitKeyByGoods();
	/**
	 * 生成订单商品
	 */
	public function buildOrderGoods(array $orderData);
	/**
	 * 生成订单商品->立即购买
	 */
	public function placeTheOrderGoods(array $order): bool;
	/**
	 * 更新订单商品状态
	 */
	public function updateOrderGoodsStatus(array $ids): bool;
	/**
	 * 根据订单信息查询订单商品信息list
	 */
	public function getOrderGoodsByInfo(array $data, string $splitKey): array;
	/**
	 * 根据订单信息查询订单商品信息（单条数据）
	 */
	public function getOrderGoodsById(array $data): array;
	/**
	 * 普通订单退换货
	 *
	 * @return boolean
	 */
	public function updateRetuernGoodsReceiveStatus(array $data): bool;
	/**
	 * 订单商品签收
	 *
	 * @return array
	 */
	public function setOrderOverTime(array $post): bool;
	/**
	 * 根据订单id及其商品id获取订单商品数据
	 *
	 * @return array
	 */
	public function getOrderPackageGoodsInfoByOAndGId(array $post): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 取消订单
	 *
	 * @return bool
	 */
	public function cancelOrderGooods(array $data): bool;
	/**
	 * 根据订单号查询 商品数据，用于在购买
	 *
	 * @return array
	 */
	public function getGoodsByOrderId(array $post): array;
	/**
	 * 删除订单商品
	 *
	 * @return bool
	 */
	public function deleteOrderGooods(array $data): bool;
	/**
	 * 多个评论修改状态
	 *
	 * @param array $post        	
	 * @return bool
	 */
	public function editOrderManyCommentStatus(array $post): bool;
	/**
	 * 多个评论修改状态
	 *
	 * @param array $post        	
	 * @return bool
	 */
	public function editOrderCommentStatus(array $post): bool;
	/**
	 * 查询购买数量
	 *
	 * @return array
	 */
	public function getPurchaseQuantity(array $data, string $splitKey): array;
	/**
	 * 计算购买数量
	 */
	public function statisticalPurchaseQuantity(array $data, string $splitKey): bool;
	/**
	 * 修改订单商品状态
	 */
	public function returnAuditStatus(array $data): int;
	/**
	 * 修改申请售后状态
	 *
	 * @param array $data        	
	 * @return bool
	 */
	public function returnAuditStatusByTrancestation(array $data): bool;
	/**
	 * 订单换货
	 * 
	 * @return boolean
	 */
	public function updateExchangeGoodsStatus(array $data): bool;
}