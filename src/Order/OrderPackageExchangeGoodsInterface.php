<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageExchangeGoodsInterface extends AbstractInterface {
	
	/**
	 * 返回验证数据
	 */
	public function getValidateByNumber();
	public function setCourierNumber(): bool;
	/**
	 * 退换货处理
	 * 
	 * @return bool
	 */
	public function applyForAfterSale(array $post): int;
	/**
	 * 返回验证数据
	 */
	public function getValidateByOrderAfterSale(): array;
}