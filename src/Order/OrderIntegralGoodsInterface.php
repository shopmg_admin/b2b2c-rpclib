<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderIntegralGoodsInterface extends AbstractInterface {
	/**
	 */
	public function getClientDataReturn();
	
	/**
	 * add 积分商品
	 */
	public function addOrderIntegral(array $data);
	/**
	 * 更新订单商品状态
	 */
	public function updateOrderGoodsStatus(array $ids): bool;
	/**
	 * 后期优化
	 */
	public function getOrderGoodsByInfo(array $data, string $splitKey): array;
	/**
	 * 根据id获取积分商品
	 *
	 * @return array
	 */
	public function getOrderGoodsById(array $data): array;
	/**
	 * 商品相关key
	 *
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 店铺关联数据
	 *
	 * @return string
	 */
	public function getSplitStore(): string;
	/**
	 * 订单商品签收
	 *
	 * @return array
	 */
	public function setOrderOverTime(array $post): bool;
	/**
	 * 查询购买数量
	 *
	 * @return array
	 */
	public function getPurchaseQuantity(array $data, string $splitKey): array;
	/**
	 * 计算购买数量
	 */
	public function statisticalPurchaseQuantity(array $data, string $splitKey): bool;
	/**
	 * 修改订单商品状态
	 */
	public function returnAuditStatus(array $data): int;
	/**
	 * 修改申请售后状态
	 *
	 * @param array $data        	
	 * @return bool
	 */
	public function returnAuditStatusByTrancestation(array $data): bool;
	/**
	 * 订单换货
	 * 
	 * @return boolean
	 */
	public function updateExchangeGoodsStatus(array $data): bool;
}