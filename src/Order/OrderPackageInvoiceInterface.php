<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageInvoiceInterface extends AbstractInterface {
	public function getValidateByAddAreRaised();
	
	
	
	public function getAllInvoiceInfo();
	public function getInvoiceInfoByOrder();
	public function getInvoicesOrderAdd();
	public function getInvoicesOrderSave();
	/**
	 * 发票更新
	 */
	public function updateInvoice(array $data): bool;
	/**
	 * 获取发票信息
	 * 
	 * @return string
	 */
	public function getInvoice(array $data): array;
	/**
	 * 获取发票信息
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function isOpenInvoice(array $data): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByType(): string;
}