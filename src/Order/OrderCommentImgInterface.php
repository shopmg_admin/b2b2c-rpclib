<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderCommentImgInterface extends AbstractInterface {
	public function getSlaveField();
	/**
	 * 获取一个商品的评论图片
	 * 
	 * @return array
	 */
	public function getOrderCommentImage(array $data): array;
	/**
	 * 获取多个商品的评论图片
	 * 
	 * @return array
	 */
	public function getOrderCommentsImage(array $data): array;
	/**
	 * 多个商品评论
	 */
	public function addImageByManyGoods(array $post, array $addNumber): bool;
	/**
	 *
	 * @param string $insertId        	
	 * @return bool
	 */
	public function addCommentPic(array $data): bool;
}