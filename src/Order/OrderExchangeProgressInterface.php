<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderExchangeProgressInterface extends AbstractInterface {
	
	/**
	 * 添加审核进度
	 *
	 * @return bool
	 */
	public function addOrderReview(array $data): bool;
	/**
	 * 获取当前退货单审核进度
	 *
	 * @return array
	 */
	public function exchangeInfo(array $data): array;
}