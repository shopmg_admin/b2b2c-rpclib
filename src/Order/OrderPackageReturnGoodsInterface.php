<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageReturnGoodsInterface extends AbstractInterface {
	public function setCourierNumber(array $post): bool;
	public function getProgressQuery();
	/**
	 *
	 * @name 退货--退货详情
	 */
	public function getOrderReturnDetails(array $post);
	/**
	 * 搜索
	 * 
	 * @param array $data        	
	 * @return array
	 */
	public function getSearchOrder(array $post, array $data): array;
	/**
	 * 获取订单关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByOrderId(): string;
	/**
	 * 获取订单关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByGoodsId(): string;
	/**
	 * 获取店铺关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByStoresId(): string;
	/**
	 * 退换货处理
	 * 
	 * @return bool
	 */
	public function applyForAfterSale(array $post): int;
	/**
	 * 返回验证数据
	 */
	public function getValidateByOrderAfterSale(): array;
}