<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderWxpayInterface extends AbstractInterface {
	
	/**
	 * 余额充值处理订单号
	 */
	public function getResultByPay(array $data): string;
	/**
	 * 获取微信订单凭据
	 */
	public function getOrderWxpayCredentials(array $data): array;
	/**
	 * 处理微信订单
	 *
	 * @param array $info        	
	 * @return int
	 */
	public function parseOrderByWx(array $info, int $type): string;
	/**
	 * 获取凭据
	 */
	public function getOrderWx(array $info, int $type);
	
	/**
	 * 微信回调更新微信订单号
	 */
	public function nofityUpdate(array $param): bool;
	/**
	 * 微信回调更新微信订单号(余额充值及其 开店)
	 */
	public function nofityUpdateBySpecial(array $param);
}