<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderReturnGoodsInterface extends AbstractInterface {
	
	/**
	 *
	 * @name 退货--退货列表
	 *      
	 */
	public function getOrderReturnList(array $post);
	/**
	 * 获取搜索条件
	 *
	 * @return array
	 */
	public function getValidateBySearch(): array;
	/**
	 *
	 * @name 退货--退货详情
	 */
	public function getOrderReturnDetails(array $post);
	/**
	 * 获取搜索列表
	 *
	 * @param array $post        	
	 * @param array $condition        	
	 * @return array
	 */
	public function getDataListBySearch(array $post, array $condition): array;
	/**
	 * 获取订单关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByOrderId(): string;
	/**
	 * 获取订单关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByGoodsId(): string;
	/**
	 * 获取店铺关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByStoresId(): string;
	/**
	 * 退换货处理
	 * 
	 * @return bool
	 */
	public function applyForAfterSale(array $post): int;
	/**
	 * 返回验证数据
	 */
	public function getValidateByOrderAfterSale(): array;
}