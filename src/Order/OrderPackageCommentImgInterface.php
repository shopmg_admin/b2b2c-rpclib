<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageCommentImgInterface extends AbstractInterface {
	
	/**
	 *
	 * @param string $insertId        	
	 * @return bool
	 */
	public function addCommentPic(array $data): bool;
}