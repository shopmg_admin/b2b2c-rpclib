<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderInterface extends AbstractInterface {
	
	
	public function getOrderStatusNum(): array;
	public function getOrderNum(array $where): int;
	public function getOrder(array $post): array;
	/**
	 * 待付款订单
	 */
	public function getPendingPaymentOrder(array $post): array;
	/**
	 * 待发货 订单
	 */
	public function getPendingDelivery(array $post): array;
	/**
	 * 待收货 订单
	 */
	public function getGoodsToBeReceived(array $post): array;
	/**
	 * 待评价订单 订单
	 */
	public function getToBeEvaluated(array $post): array;
	/**
	 * 已评价评价订单 订单
	 */
	public function getAlreadyBeEvaluated(array $post): array;
	/**
	 * 已完成订单
	 */
	public function getCompleted(array $post): array;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getSearchOrderKey()
	 */
	public function getSearchOrderKey(): array;
	public function getOrderCommonNum();
	public function getDelOrderByList(array $data): bool;
	public function getCancelOrderByList(array $post): bool;
	public function getOrderDetail(array $post): array;
	/**
	 * 多商品生成订单
	 * 判断订单总金额
	 * 判断每个商品的库存
	 * 生成订单总订单
	 * 将商品信息分发到订单商品表
	 * 将购物车信息进行修改
	 */
	public function multiCommodityGeneratedOrder(array $data): array;
	/**
	 * 立即购买->下单
	 */
	public function placeTheOrder(array $params): array;
	/**
	 * 提交事务
	 *
	 * @return bool
	 */
	public function submitTranstion(): void;
	/**
	 * 立即购买验证参数
	 *
	 * @return array
	 */
	public function getValidateByGoods(): array;
	/**
	 * 配件检查参数
	 */
	public function getMessageValidateByParts(): array;
	/**
	 * 支付回调成功后 修改订单状态
	 */
	public function paySuccessEditStatus(array $orderId, array $payConfig): bool;
	/**
	 * 已签收
	 *
	 * {@inheritdoc}
	 *
	 */
	public function getOverTime(array $post): bool;
	/**
	 * 个人中心--删除订单
	 */
	public function delOrders(array $post): bool;
	public function getOrderSnByData(array $data, string $splitKey): array;
	/**
	 * dange评论状态
	 *
	 * @return bool
	 */
	public function commentStatus(array $post): bool;
	/**
	 * 根据退货信息获取订单号及其邮费信息
	 *
	 * @return array
	 */
	public function getOrderDetailByOrderReturnRetuernGoods(array $data, string $splitKey): array;
	/**
	 * 确认收货
	 *
	 * @author 王强
	 */
	public function confirmGetgoods(array $post): bool;
	/**
	 * 店铺关联数据
	 *
	 * @return string
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 地址关联key
	 *
	 * @return string
	 */
	public function getSplitKeyByAddress(): string;
	public function getSplitKeyByPayType(): string;
	public function getSplitKeyByExp(): string;
	/**
	 * 检查是否已支付
	 */
	public function isCheckPay(array $post): bool;
	/**
	 * 查询订单状态
	 * 
	 * @return array
	 */
	public function selectorderStatus(array $post): array;
	/**
	 * 检查数据
	 * 
	 * @return array
	 */
	public function getValidateByOrderCheck(): array;
	/**
	 * 组装支付数据
	 *
	 * @return array
	 */
	public function orderPayBuildData(array $post): bool;
}