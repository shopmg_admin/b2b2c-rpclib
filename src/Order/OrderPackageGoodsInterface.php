<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageGoodsInterface extends AbstractInterface {
	public function buildOrderGoods(array $data): bool;
	
	/**
	 * 更新订单商品状态
	 */
	public function updateOrderGoodsStatus(array $data);
	/**
	 * 获取套餐订单商品
	 * 
	 * @return array
	 */
	public function getPackageOrderGoodsInfo(array $data, string $splitKey): array;
	/**
	 *
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 根据订单id获取商品相关
	 */
	public function getOrderGoodsById(array $data): array;
	/**
	 * 根据订单id及其商品id获取订单商品数据
	 * 
	 * @return array
	 */
	public function getOrderPackageGoodsInfoByOAndGId(array $post): array;
	/**
	 * 套餐换货 修改状态
	 */
	public function updateExchangeGoodsStatus(array $data): bool;
	/**
	 * 套餐退货 修改状态
	 */
	public function updateReturnGoodsStatus(array $data): bool;
	/**
	 * 普通订单退换货
	 * 
	 * @return boolean
	 */
	public function updateRetuernGoodsReceiveStatus(array $post): bool;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getMessageNotice()
	 */
	public function getMessageNotice(): array;
	/**
	 *
	 * @return array
	 */
	public function getSplitKeyByStore(): string;
	/**
	 * 订单商品签收
	 * 
	 * @return array
	 */
	public function setOrderOverTime(array $post): bool;
	/**
	 * 返回验证数据
	 */
	public function getValidateByApply(): array;
	/**
	 * 修改订单商品状态(确认收货)
	 */
	public function confirmGet(array $data): bool;
	/**
	 * 查询购买数量
	 *
	 * @return array
	 */
	public function getPurchaseQuantity(array $data, string $splitKey): array;
	/**
	 * 计算购买数量
	 */
	public function statisticalPurchaseQuantity(array $data, string $splitKey): bool;
	/**
	 * 修改订单商品状态
	 */
	public function returnAuditStatus(array $data): int;
	/**
	 * 修改申请售后状态
	 *
	 * @param array $data        	
	 * @return bool
	 */
	public function returnAuditStatusByTrancestation(array $data): bool;
}