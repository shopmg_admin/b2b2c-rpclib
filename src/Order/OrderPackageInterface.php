<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderPackageInterface extends AbstractInterface {
	/**
	 *
	 * @return array
	 */
	public function getPayData(): array;
	/**
	 * 发票信息
	 * 
	 * @return array
	 */
	public function getInvoiceData(): array;
	/**
	 * 获取订单编号
	 * 
	 * @return number
	 */
	public function getOrderId();
	
	
	public function getValidateByCancel();
	
	/**
	 * 产生订单--立即购买
	 */
	public function orderBegin(array $data): array;
	/**
	 * /**
	 * 取消订单
	 */
	public function cancel_order(array $data): bool;
	/**
	 * 支付回调成功后 修改订单状态
	 */
	public function paySuccessEditStatus(array $data, array $payConfig);
	public function getOrderDel(array $data): bool;
	/**
	 * 提交事务
	 * 
	 * @return bool
	 */
	public function submitTranstion(): void;
	/**
	 * 获取待付款订单
	 * 
	 * @return array
	 */
	public function getPendingPaymentOrderList(array $post): array;
	/**
	 * 获取全部列表（删除除外）
	 * 
	 * @return array
	 */
	public function geListAllIgnoreDel(array $post): array;
	/**
	 * 获取待发货订单
	 */
	public function getPendingDelivery(array $post): array;
	/**
	 * 获取待收货订单
	 */
	public function getGoodsToBeReceived(array $post): array;
	/**
	 * 获取待评价订单
	 */
	public function getToBeEvaluated(array $post): array;
	/**
	 * 获取已评价评价订单
	 */
	public function getAlreadyEvaluated(array $post): array;
	/**
	 * 获取已取消订单
	 */
	public function getHaveBeenCancelled(array $post): array;
	public function getOrder(array $post): array;
	/**
	 * 获取完成订单
	 */
	public function getCompleted(array $post): array;
	/**
	 * 获取订单根据id
	 */
	public function getOrderDetail(array $post): array;
	public function getSplitKeyByAddress(): string;
	public function getSplitKeyByPayType(): string;
	public function getSplitKeyByExp(): string;
	public function getSplitKeyByStore(): string;
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\Models\Logic\AbstractLogic::getSearchOrderKey()
	 */
	public function getSearchOrderKey(): array;
	/**
	 * 评论状态
	 * 
	 * @return bool
	 */
	public function commentStatus(array $data): bool;
	/**
	 * 已签收
	 * 
	 * {@inheritdoc}
	 *
	 */
	public function getOverTime(array $post): bool;
	/**
	 * 确认收货
	 * 
	 * @author 王强
	 */
	public function confirmGetgoods(array $data): bool;
	/**
	 * 获取套餐订单数据
	 * 
	 * @param array $data        	
	 * @param string $splitKey        	
	 * @return array
	 */
	public function getOrderSnByData(array $data, string $splitKey): array;
	/**
	 * 检查是否已支付
	 */
	public function isCheckPay(array $post): bool;
	/**
	 * 查询订单状态
	 * 
	 * @return array
	 */
	public function selectorderStatus(array $post): array;
	/**
	 * 检查数据
	 * 
	 * @return array
	 */
	public function getValidateByOrderCheck(): array;
	/**
	 * 组装支付数据
	 *
	 * @return array
	 */
	public function orderPayBuildData(array $post): bool;
}