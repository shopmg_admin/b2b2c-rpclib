<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface CommonOrderApplyForAfterSaleInterface extends AbstractInterface {
	public function setCourierNumber(): bool;
	/**
	 * 返回验证数据
	 */
	public function getValidateByNumber(): array;
	/**
	 * 退换货处理
	 * 
	 * @return bool
	 */
	public function applyForAfterSale(array $post): int;
	/**
	 * 返回验证数据
	 */
	public function getValidateByOrderAfterSale(): array;
}