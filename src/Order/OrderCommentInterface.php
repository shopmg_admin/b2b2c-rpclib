<?php

declare ( strict_types = 1 )
	;

namespace Lib\Order;

use Lib\AbstractInterface;

interface OrderCommentInterface extends AbstractInterface {
	public function getEvaluateDetails(array $post);
	/**
	 * 获取商品分割键
	 *
	 * @return string
	 */
	public function getSplitKeyByGoods(): string;
	/**
	 * 分组求数量
	 */
	public function getCommentsListGroupCount(array $post): array;
	/**
	 * 用户关联key
	 * 
	 * @return string
	 */
	public function getSplitKeyByUser(): string;
	/**
	 * 获取各个商品的评分
	 */
	public function getGoodsRecommend(array $post): array;
	/**
	 * 根据评分推荐商品
	 */
	public function getRecommend(): array;
	/**
	 * 多个商品评价
	 */
	public function manyGoodsComment(array $post): array;
	/**
	 * 验证评论数据
	 * 
	 * @return array
	 */
	public function validateDataByComment(): array;
	/**
	 * 添加评价
	 * 
	 * @return bool
	 */
	public function recordConmment(array $data): int;
}