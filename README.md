# b2b2c-rpclib

#### 介绍
>> rpc 服务 interface

#### 软件架构

>> 多商户微服务rpc-lib 提供统一interface


#### 安装教程

>> composer require opjklu/rpclib 

#### 使用说明

 >> 配合rpc 服务端与客户端桥接使用

#### 参与贡献

>>  Fork 本仓库
>>  新建 Feat_xxx 分支
>>  提交代码
>>  新建 Pull Request


#### 米糕网络科技

>>  **旨在为客户带来新零售、o2o、b2c、b2b2c、s2b2c、f2b2c、批发等其他的解决方案，为客户拓宽销售渠道，提高收益**

>> **我们期待与您的合作**

>>  如有需要请加QQ 1968637674 

>> 支持请点Star
